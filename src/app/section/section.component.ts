import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { UntypedFormControl, UntypedFormBuilder, UntypedFormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { startWith, map, take } from 'rxjs/operators';
import { ApiConnectionService } from '../api-connection.service';
import { WasUp } from '../popover/wasup/wasup.dialog';
import { WasAlert } from '../popover/wasalert/wasalert.dialog';
import { LCCSection, LCCSong } from '../structs';

@Component({
  selector: 'lct-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
  // Add event emitter to remove this section
  @Output() removeSection = new EventEmitter<number>();
  @Input() public autocomplete: string[] = [];
  @Input() public index = 0;
  @Input() public isExpanded = false;
  @Input() public sectionForm = this.getNewBlankSection();
  filteredSongs: Observable<string[]>;
  panelOpenState = true;
  backgroundColor = '#2e1100';  // (46, 17, 0, 1)
  textColor = '#fffed0';  // (255, 254, 208, 1)
  viewEnabled = false;
  slideStyle: any | undefined = undefined;

  constructor(public api: ApiConnectionService, @Inject(UntypedFormBuilder) private fb: UntypedFormBuilder, public dialog: MatDialog) {

    this.filteredSongs = this.sectionIdCtrl.valueChanges
      .pipe(
        startWith(''),
        map(state => state ? this.filterSongs(state) : this.autocomplete.slice())
      );
  }

  ngOnInit(): void {


    this.filteredSongs = this.sectionIdCtrl.valueChanges
      .pipe(
        startWith(''),
        map(state => state ? this.filterSongs(state) : this.autocomplete.slice())
      );
    // console.log(this.sectionForm);
    // console.log(this.sectionPrependBlankCtrl);
    // console.log(this.sectionAppendBlankCtrl);
    // console.log(this.sectionIsNewCtrl);
  }

  onCheckboxChange(event: any) {
    // console.log(event);
    if (event.checked) {
      this.slideStyle = { "background": this.backgroundColor, "color": this.textColor };
    } else {
      this.slideStyle = undefined;
    }
  }

  onColorPickerChangeBackground(event: Event) {
    this.backgroundColor = (event.target as HTMLInputElement).value;
    console.log('Color picker value changed:', this.backgroundColor);
    // Do something with the new color value
  }
  onColorPickerChangeFont(event: Event) {
    this.textColor = (event.target as HTMLInputElement).value;
    console.log('Color picker value changed:', this.textColor);
  }

  get sectionIdCtrl() {
    return this.sectionForm.get('id') as UntypedFormControl;
  }

  get sectionTextCtrl() {
    return this.sectionForm.get('text') as UntypedFormControl;
  }
  get sectionOptionsGrp() {
    return this.sectionForm.get('options') as UntypedFormGroup;
  }
  get sectionPrependBlankCtrl() {
    return this.sectionForm.get('options')!.get('prependBlank') as UntypedFormControl;
  }
  get sectionAppendBlankCtrl() {
    return this.sectionForm.get('options')!.get('appendBlank') as UntypedFormControl;
  }
  get sectionIsNewCtrl() {
    return this.sectionForm.get('options')!.get('isNew') as UntypedFormControl;
  }
  get sectionAddLicenseCtrl() {
    return this.sectionForm.get('options')!.get('addLicense') as UntypedFormControl;
  }
  get sectionBackgroundColorCtrl() {
    return this.sectionForm.get('options')!.get('backgroundColor') as UntypedFormControl;
  }
  get sectionTextColorCtrl() {
    return this.sectionForm.get('options')!.get('textColor') as UntypedFormControl;
  }


  // This function returns a blank section form
  getNewBlankSection() {
    return this.fb.group({
      id: ['', Validators.required],  // Validators.maxLength(64)
      text: ['', Validators.required],
      options: this.fb.group({
        prependBlank: [false],
        appendBlank: [true],
        isNew: [false],
        addLicense: [true],
        backgroundColor: this.backgroundColor,
        textColor: this.textColor
      })
    });
  }


  filterSongs(name: string) {
    // console.log(this.autocomplete);
    return this.autocomplete.filter(song =>
      song.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  addSong() {
    // this.addSongTitle.emit(this.sectionIdCtrl.value);
    this.addSongTitle(this.sectionIdCtrl.value);
  }

  removeMe() {
    this.removeSection.emit(this.index);
  }

  addSongTitle(id: string) {
    console.log('addSongTitle', id);
    this.api.getSong(id).subscribe({
      next: (song) => {
        // console.log('autocomplete', autoList);
        if (song && song.text) {
          this.sectionTextCtrl.setValue(song.text);
        }
      },
      error: (err: Error) => {
        // console.error('api: getSong: error: ' + err);
        this.dialog.open(WasAlert, { data: { title: 'Not Found', icon: 'info', body: 'Song not found, add text manually and if desired check New Song to save to database.', buttons: ['Ok'] } });
      },
      complete: () => {
        console.log('api: getSong: Observer got a complete notification');
      }
    });
  }

}
