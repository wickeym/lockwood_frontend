import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { LCCSection } from '../structs';


/**
 * @title Autocomplete overview https://v5.material.angular.io/components/autocomplete/examples
 */
@Component({
  selector: 'lct-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent {

  stateCtrl: UntypedFormControl;
  filteredSongs: Observable<string[]>;

  @Input() public autocomplete: string[] = [];
  @Output() addSongTitle = new EventEmitter<string>();
  songId: string = '';

  constructor() {
    this.stateCtrl = new UntypedFormControl();
    this.filteredSongs = this.stateCtrl.valueChanges
      .pipe(
        startWith(''),
        map(state => state ? this.filterSongs(state) : this.autocomplete.slice())
      );
  }

  filterSongs(name: string) {
    return this.autocomplete.filter(song =>
      song.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  addSong() {
    this.addSongTitle.emit(this.stateCtrl.value);
  }

}
