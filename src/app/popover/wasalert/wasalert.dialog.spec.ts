import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WasAlert } from './wasalert.dialog';

describe('WasalertComponent', () => {
  let component: WasAlert;
  let fixture: ComponentFixture<WasAlert>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WasAlert]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WasAlert);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
