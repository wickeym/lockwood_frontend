import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wasup } from './wasup.dialog';

describe('WasupComponent', () => {
  let component: Wasup;
  let fixture: ComponentFixture<Wasup>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Wasup]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wasup);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
