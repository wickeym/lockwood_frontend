import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WasLogin } from './waslogin.dialog';

describe('WasalertComponent', () => {
  let component: WasLogin;
  let fixture: ComponentFixture<WasLogin>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WasLogin]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WasLogin);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
