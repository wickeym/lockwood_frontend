import { Component, Input, Inject, OnInit } from '@angular/core';
import { FormControl, UntypedFormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ApiConnectionService } from 'src/app/api-connection.service';
import { UserService } from 'src/app/user.service';


@Component({
  selector: 'lct-waslogin-dialog',
  templateUrl: './waslogin.dialog.html',
  styleUrls: ['./../popover.css'],
})
export class WasLogin implements OnInit {
  /**@ignore*/
  public add_class = 'wasup-title-sso';
  loginForm = this.fb.group({
    email: ['', [Validators.email, Validators.required, Validators.maxLength(64)]],
    password: ['', [Validators.required, Validators.maxLength(64)]],
    createAccount: [false]
  });
  user: any = {};
  public version = '0.0.1';
  hide = true;
  /**
   * Create simple modal popup with email and password form.
   *
   * SIMPLE USE CASE
    this.dialog.open(WasLogin, {data: {}}).afterClosed().subscribe(result => {
      if (result === 1) {
        console.log('log in this user');
      }
      else {
        console.log('log out this user');
        this.dialog.open(WasUp, { data: { title: 'Logged Out', icon: 'done', body: 'You have successfully logged out.' } });
      }
    });
   *
   * @example
    import { WasLogin } from 'wickeyappstore';
    import { MatDialog, MatDialogRef } from '@angular/material';
    Inject MatDialog in the constructor(public dialog: MatDialog) { }
   */
  constructor(
    public dialogRef: MatDialogRef<WasLogin>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: UntypedFormBuilder, public api: ApiConnectionService,
    public breakpointObserver: BreakpointObserver,
    public userService: UserService) {
    // console.log('print WasLogin ', dialogRef);
    // SET DEFAULT VALUES
    dialogRef.disableClose = true; // do not close by clicking off by default
  }
  /** @ignore */
  rebuildForms() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.email, Validators.required, Validators.maxLength(64)]],
      password: ['', [Validators.required, Validators.maxLength(64)]]
    });
  }

  /** @ignore */
  ngOnInit() {
    // https://material.angular.io/cdk/layout/overview
    this.breakpointObserver.observe([
      Breakpoints.Handset,
      Breakpoints.Tablet
    ]).subscribe(result => {
      if (result.matches) {
        // NOTE: IFF mobile, set size to full screen
        this.dialogRef.updateSize('100%', '100%');
        this.dialogRef.addPanelClass('was-modal-m');
        this.add_class = 'wasup-title-sso-m';
      }
    });
  }

  loginUser(): void {
    console.log('loginUser: login');
    this.userService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe({
      next: (data) => {
        console.log('login: next', data);
        this.user = data;
        this.dialogRef.close(true);
      },
      error: (err: Error) => {
        console.error('login: error: ', err);
        if (err.name === 'Unauthorized') {
          this.rebuildForms();
        } else if (err.name === 'Forbidden') {
          this.rebuildForms();
        }
      },
      complete: () => {
        console.log('login: Observer got a complete notification');
      }
    });
  }

  logoutClick(): void {
    console.log('logoClick');
    this.userService.logout();
  }

  getProfile(): void {
    console.log('getProfile');
    this.userService.profile().subscribe({
      next: (data) => {
        console.log('getProfile: next', data);
        this.user = data;
      },
      error: (err: Error) => {
        console.error('api: getProfile: error: ' + err);
      },
      complete: () => {
        console.log('api: getProfile: Observer got a complete notification');
      }
    });
  }
  setProfile(): void {
    console.log('getProfile');
    this.api.updateProfile({ first_name: 'Michael' }).subscribe({
      next: (data) => {
        console.log('updateProfile: next', data);
        this.user = data;
      },
      error: (err: Error) => {
        console.error('api: updateProfile: error: ' + err);
      },
      complete: () => {
        console.log('api: updateProfile: Observer got a complete notification');
      }
    });
  }

  /**
   * Cancel/close the dialog
   *
   * @memberof WasLogin
   */
  onNoClick(): void {
    this.dialogRef.close();
  }
}
