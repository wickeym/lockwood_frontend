import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LCTMenu } from './lctmenu.component';

describe('LCTMenu', () => {
  let component: LCTMenu;
  let fixture: ComponentFixture<LCTMenu>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LCTMenu]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LCTMenu);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
