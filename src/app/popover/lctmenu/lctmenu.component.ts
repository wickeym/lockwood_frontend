import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from 'src/app/user.service';
import { WasUp } from '../wasup/wasup.dialog';
import { UserModel } from 'src/app/structs';
/**
* LCT Interface

* This is <b>REQUIRED</b> and is the hook into LCT login and other functionality.
*
* SIMPLY ADD to HTML in your main app page
* ```js
* <lct-wasmenu></lct-wasmenu>
* ```
* @ignore
*/
@Component({
  selector: 'lct-menu',
  templateUrl: './lctmenu.component.html',
  styleUrls: ['./../popover.css'],
})
export class LCTMenu implements OnInit {
  /** Event emitted when the associated menu is opened.*/
  @Output() open = new EventEmitter<void>();

  /**@ignore*/
  public hasInapps = false;
  /**@ignore*/
  constructor(public userService: UserService, public dialog: MatDialog) {
  }
  ngOnInit(): void {
  }
  /**@ignore*/
  get loginMessage() {
    return this.userService.isLoggedIn.pipe(map((_isLogged: Boolean) => {
      if (_isLogged) {
        return 'Logout';
      } else {
        return 'Login';
      }
    }));
  }
  /**@ignore*/
  get infoIcon() {
    return this.userService.isLoggedIn.pipe(map((_isLogged: Boolean) => {
      if (_isLogged) {
        return 'info';
      } else {
        return 'help';
      }
    }));
  }
  /**@ignore*/
  leavereview() {
  }
  /**@ignore*/
  openshop() {
  }
  /**@ignore*/
  openlogin() {
    this.userService.openLogin().subscribe(res => {
      if (res) {
        this.dialog.open(WasUp, {
          width: '300px', data: { title: 'Attention', icon: 'done', body: 'Successfully logged in', stayopen: false }
        });
      }
    });
  }
  /**@ignore*/
  openAccountCreate() {
    this.userService.openAccountCreate().subscribe(res => {
      if (res) {
        this.dialog.open(WasUp, {
          width: '300px', data: { title: 'Attention', icon: 'done', body: 'Successfully logged in', stayopen: false }
        });
      }
    });
  }
  /**@ignore*/
  openuserinfo() {
    this.openlogin();
    // this.userService.openuserinfo();
  }
  /**@ignore*/
  openEvent() {
    this.open.emit();
  }

}
