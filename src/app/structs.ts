export interface AppData {
  title: string;
  description: string;
  version: string;
  askToUpdate: boolean;
}

export interface LCCSong {
  id: string;
  attribution?: string;
  text: string;
  is_scripture?: boolean;
  is_hymn?: boolean;
  hymn_number?: number;
  license?: string;
  usage?: number;
  last_modified?: number;
  created_time?: number;
}

export interface LCCSectionOptions {
  prependBlank?: boolean;
  appendBlank?: boolean;
  isNew?: boolean;
  addLicense?: boolean;
  backgroundColor?: string;
  textColor?: string;
}

export interface LCCSection {
  id: string;
  text: string;
  options: LCCSectionOptions;
}

export interface LCCPPT {
  id: string;
  sections: LCCSection[];
  usage?: number;
  last_modified?: number;
  created_time?: number;
}

export class WasAlertData {
  title: string = '';
  body: string = '';
  input: boolean = false;
  password: boolean = false;
  buttons: string[] = ['Ok'];
  button_icons: string[] = [''];
  button_colors: string[] = [''];
  list?: string[];
  input_value?: string;
  style?: string;
  image?: string;
}

export class WasUpData {
  title: string = '';
  body: string = '';
  icon: string = 'warning';
  stayopen: boolean = false;
}


export class UserModel {
  user_id: string;
  is_anonymous: boolean;
  logging_in: boolean;
  status = 200;  // HTTP status
  coins = 0;
  email?: string;
  phone_number?: string;
  first_name?: string;
  last_name?: string;
  zip_code?: string;
  bs_id?: number;
  last_modified?: number;
  created_time?: number;
  push_id?: string;
  account_verified = false;
  freebie_used = false;
  rated_app = false;
  version = .1;
  constructor(user_id?: string) {
    if (user_id) {
      this.user_id = user_id;
    } else {
      this.user_id = '';
    }
    this.is_anonymous = true;
    this.logging_in = false;
  }
  update(_api_return: any): void {
    // This assumes that this is only called with a real user, as apposed to an anonymous user.
    this.user_id = _api_return.user_id;
    this.is_anonymous = false;
    if (_api_return.hasOwnProperty('email')) {
      this.email = _api_return.email;
    }
    if (_api_return.hasOwnProperty('coins')) {
      this.coins = _api_return.coins;
    }
    if (_api_return.hasOwnProperty('created_time')) {
      this.created_time = _api_return.created_time;
    }
    if (_api_return.hasOwnProperty('freebie_used')) {
      this.freebie_used = _api_return.freebie_used;
    }
    if (_api_return.hasOwnProperty('last_modified')) {
      this.last_modified = _api_return.last_modified;
    }
    if (_api_return.hasOwnProperty('version')) {
      this.version = _api_return.version;
    }
    if (_api_return.hasOwnProperty('zip_code')) {
      this.zip_code = _api_return.zip_code;
    }
    // IN SCRAPE //
    if (_api_return.hasOwnProperty('last_name')) {
      this.last_name = _api_return.last_name;
    }
    if (_api_return.hasOwnProperty('first_name')) {
      this.first_name = _api_return.first_name;
    }
    if (_api_return.hasOwnProperty('phone_number')) {
      this.phone_number = _api_return.phone_number;
    }
    if (_api_return.hasOwnProperty('bs_id')) {
      this.bs_id = _api_return.bs_id;
    }
  }
}
export class UserParams {
  user_id?: string;
  coins?: number;
  email?: string;
  freebie_used?: boolean;
  rated_app?: boolean;
  logging_in?: boolean;
  push_id?: string;
  first_name?: string;
  last_name?: string;
  zip_code?: string;
  phone_number?: string;
  standalone?: boolean;
  version?: number;
  lang?: string;
  new_account?: boolean;
  verification_token?: string;
  password?: string;
}

export interface AuthResult {
  idToken: string;
  expiresIn: number;
  user: UserModel;
  account_created?: boolean;
}

export interface ErrorTable {
  title: string;
  message: string;
  header_bg?: string;
  header_color?: string;
  button_type?: string;
  randcookie?: string;
  video?: string;
  button_action?: string;
  helpmessage?: Array<Array<string>>; // Only can have up to 3 //
}

export interface WasAlertTable {
  title: string;
  text: string;
  gif?: string;
  btn?: {
    title: string;
    action: string;
  };
}
