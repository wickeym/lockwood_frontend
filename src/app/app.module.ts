import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// MATERIAL
// UNUSED MATERIAL MODULES //
// MatAutocompleteModule, MatButtonToggleModule, MatDatepickerModule,
// MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatRadioModule,
// MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule,
// MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule,
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatStepperModule } from '@angular/material/stepper';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropModule } from '@angular/cdk/drag-drop';

// APP
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { SectionComponent } from './section/section.component';
import { LocalStorageService } from './local-storage.service';
import { WasAlert } from './popover/wasalert/wasalert.dialog';
import { WasUp } from './popover/wasup/wasup.dialog';
import { WasLogin } from './popover/waslogin/waslogin.dialog';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { PptComponent } from './ppt/ppt.component';
import { AuthInterceptor } from './api-interceptor';
import { LCTMenu } from './popover/lctmenu/lctmenu.component';
import { LctCreate } from './popover/lctcreate/lctcreate.dialog';

@NgModule({
  declarations: [
    AppComponent,
    SectionComponent,
    WasAlert,
    WasUp,
    WasLogin,
    AutocompleteComponent,
    PptComponent,
    LCTMenu,
    LctCreate
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule,
    // .withOptions({
    //   cookieName: 'csrftoken',
    //   headerName: 'X-CSRFTOKEN',
    // }),
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatButtonModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDialogModule, MatExpansionModule,
    MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatRippleModule, MatFormFieldModule,
    MatProgressSpinnerModule, MatStepperModule, MatToolbarModule, MatAutocompleteModule, MatSlideToggleModule, DragDropModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
