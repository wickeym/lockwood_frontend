import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SwUpdate } from '@angular/service-worker';
import { WasAlert } from './popover/wasalert/wasalert.dialog';
import { WasLogin } from './popover/waslogin/waslogin.dialog';
import { WasUp } from './popover/wasup/wasup.dialog';
import { AppData } from './structs';
import { ApiConnectionService } from './api-connection.service';
import { UserService } from './user.service';

@Component({
  selector: 'lct-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'lockwood_frontend';
  autocomplete: string[] = [];
  songId: string = '';
  // Create a section component that shows the form, title, append/prepend blank slide, and text box
  // the title will use autocomplete list from server to fill text box if one is found
  //
  // add button to add another section form
  // add button to create ppt (this sends the data back to server which saves to db and writes the ppt)
  constructor(public updates: SwUpdate, public dialog: MatDialog, public api: ApiConnectionService, public userService: UserService) {
    // LISTEN/HANDLE FOR NEW SITE VERSION TEST//
    updates.versionUpdates.subscribe(evt => {
      switch (evt.type) {
        case 'VERSION_DETECTED':
          console.log(`Downloading new app version: ${evt.version.hash}`, evt);
          break;
        case 'VERSION_READY':
          console.log(`Current app version: ${evt.currentVersion.hash}`);
          console.log(`New app version ready for use: ${evt.latestVersion.hash}`, evt);
          console.log('promptUser: NEW CONTENT', event);
          // event.version.name/description/version
          let app_title = 'New Content';
          let app_description = 'New or updated content is available! Please refresh your page.';
          let app_version = null;
          let app_askToUpdate = true;
          let latestAppData = (<AppData>evt.latestVersion.appData)
          if (latestAppData) {
            if (latestAppData.hasOwnProperty('title') && latestAppData.title !== null) {
              app_title = latestAppData['title'];
            }
            if (latestAppData.hasOwnProperty('description') && latestAppData['description'] !== null) {
              app_description = latestAppData['description'];
            }
            if (latestAppData.hasOwnProperty('version') && latestAppData['version'] !== null) {
              app_version = latestAppData['version'];
            }
            if (latestAppData.hasOwnProperty('askToUpdate') && latestAppData['askToUpdate'] !== null) {
              app_askToUpdate = latestAppData['askToUpdate'];
            }
          }
          if (app_version !== null) {
            app_description += ` [v${app_version}]`;
          }
          if (app_askToUpdate) {
            const dialogRef = this.dialog.open(WasAlert, {
              data: {
                title: app_title, body: app_description,
                buttons: ['Cancel', 'Refresh'], button_icons: ['cancel', 'refresh'], button_colors: ['warning', 'primary']
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result !== undefined) {
                if (result === 1) {
                  this.updates.activateUpdate().then(() => document.location.reload());
                }
              } else {
                console.log('Dialog was cancelled');
              }
            });
          }
          break;
        case 'VERSION_INSTALLATION_FAILED':
          console.log(`Failed to install app version '${evt.version.hash}': ${evt.error}`, evt);
          break;
      }
    });
    // LISTEN/HANDLE FOR NEW SITE VERSION TEST//
  }

  ngOnInit(): void {
    this.api.getAutocomplete().subscribe({
      next: (autoList) => {
        // console.log('autocomplete', autoList);
        this.autocomplete = autoList;
      },
      error: (err: Error) => {
        console.error('api: getAutocomplete: error: ' + err);
      },
      complete: () => {
        console.log('Observer got a complete notification');
      }
    });
    this.userService.loginChange.subscribe((_isLogged: boolean) => {
      console.log('loginChange', _isLogged);
    });

  }
  ngOnDestroy(): void {

  }

  createSection(section: any): void {
    console.log('createSection', section);
    // NOTE: If section.isNew then call api to create new section/song
  }

  // This function accepts the section array and calls an api to create a new ppt
  createPPT(ppt: any) {
    console.log('createPPT', ppt);
  }

  // TODO: Copy the SSO dialog to use for the create account dialog.
  // Add the sso token apis to the server.
  // On account create, get email, password, then send token to that email and on verification create the account and return the jwt token.


}
