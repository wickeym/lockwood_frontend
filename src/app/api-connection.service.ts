import { throwError, Subscription, Observable, retry } from 'rxjs';
import { share, catchError, map } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';

// import './sha1';
import { WasAlert } from './popover/wasalert/wasalert.dialog';
import { WasUp } from './popover/wasup/wasup.dialog';
import { LCCSong, LCCPPT, AuthResult, UserModel, UserParams } from './structs';


@Injectable({
  providedIn: 'root'
})
export class ApiConnectionService {
  // Big help: https://blog.angular-university.io/angular-jwt-authentication/
  // https://angular.io/guide/http
  private serverDomain = 'https://api.michaeljwickey.com';
  private autocompleteUrl = 'https://api.michaeljwickey.com/lockwood/v1/autocomplete/';
  private getSongUrl = 'https://api.michaeljwickey.com/lockwood/v1/song/';
  // These two will get a saved ppt and save a ppt to db.
  private getPPTUrl = 'https://api.michaeljwickey.com/lockwood/v1/ppt/';
  private setPPTUrl = 'https://api.michaeljwickey.com/lockwood/v1/ppt/';
  private loginUrl = `${this.serverDomain}/lockwood/v1/login/`;
  private profileUrl = `${this.serverDomain}/lockwood/v1/me/`;
  private emailVerificationTokenUrl = `${this.serverDomain}/lockwood/v1/email/token/`;
  private emailVerificationUrl = `${this.serverDomain}/lockwood/v1/email/verify/`;
  private simpleEmailVerificationUrl = `${this.serverDomain}/lockwood/v1/email/verify/simple/`;
  private changePasswordUrl = `${this.serverDomain}/lockwood/v1/me/password/change/`;
  private forgotPasswordUrl = `${this.serverDomain}/lockwood/v1/password/reset/`;

  private version: number = .1;
  private lang: string = 'english';
  private apiHeaders: HttpHeaders;
  private lastCookieString = '';
  private lastToken: string | null = null;
  private xmlparser = new DOMParser();

  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private doc: any,
    public dialog: MatDialog
  ) {
    this.apiHeaders = new HttpHeaders().set('API-VERSION', this.version.toString());
  }

  // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#The_Unicode_Problem
  // first use encodeURIComponent to get percent-encoded UTF-8 then convert the percent encodings into raw bytes which can be fed into btoa
  b64EncodeUnicode(str: string): string {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
      function toSolidBytes(match, p1) {
        return String.fromCharCode(Number('0x' + p1));
      }));
  }
  cookie_read(name: string): any {
    const result = new RegExp('(?:^|; )' + encodeURIComponent(name) + '=([^;]*)').exec(this.doc.cookie);
    return result ? result[1] : null;
  }
  cookie_write_multi(name: string, value: string | undefined, days?: number): void {
    try {
      if (!days) {
        days = 365 * 20;
      }
      const date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      const expires = '; expires=' + date.toUTCString();
      this.doc.cookie = name + '=' + value + expires + ';domain=.instaboost.com;secure;path=/;SameSite=Lax';
    } catch (cookieError) {
      console.error('cookie_write', cookieError);
    }
  }
  cookie_remove(name: string): void {
    this.cookie_write_multi(name, undefined, -1);
  }

  getHeaders(username?: string, password?: string) {
    let _headers: any = { 'API-VERSION': this.version.toString(), 'Content-Type': 'application/json' };
    if (username && password) {
      const token = btoa(`${username}:${password}`);
      _headers['Authorization'] = `Basic ${token}`;
    }
    return new HttpHeaders(_headers);
  }

  // THE OBSERVABLE WAY //
  private handleError(reserr: HttpErrorResponse) {
    console.log('handleError', reserr);
    let errMsg: string;
    if (reserr.error instanceof Error) {
      // A client-side or network error occurred.
      // http://stackoverflow.com/questions/39571231/how-to-check-whether-user-has-internet-connection-or-not-in-angular2
      if (navigator.onLine) {
        errMsg = `${reserr.status} - ${reserr.statusText || ''} ${reserr.error.message}`;
      } else {
        errMsg = 'You appear to be offline. Connect to the internet and try again.';
        // alert(errMsg);
        this.dialog.open(WasUp, {
          width: '300px', data: { title: 'Attention', icon: 'report_problem', body: errMsg, stayopen: false }
        });
      }
    } else {
      if (navigator.onLine) {
        // The backend returned an unsuccessful response code.
        // {"error": {"message": string, code: number}} // where code is a http status code as-well as an internal error code.
        try {
          const _checkIfValue = function (_obj: any, _key: string) {
            if (_obj.hasOwnProperty(_key) && (_obj[_key] !== undefined && _obj[_key] !== null)) {
              return true;
            } else {
              return false;
            }
          };
          if (_checkIfValue(reserr, 'error') && _checkIfValue(reserr.error, 'error') && _checkIfValue(reserr.error.error, 'message')) {
            errMsg = reserr.error.error.message;  // JSON.parse(error.error)
            // Catch 419 session expired error that will be returned on invalid session id
            // FOR NOW, ONLY HANDLE THE SESSION EXPIRED CASE
            if (reserr.error.error.code === 419) {
              this.dialog.open(WasAlert, {
                data: { title: 'Attention', body: errMsg, buttons: ['Cancel', 'Login'] }
              }).afterClosed().subscribe(result => {
                // result is the index of the button pressed
                if (result === 1) {
                  console.log('Open SSO');  // After SSO is a dialog, simply open right here
                }
              });
            }
          } else if (_checkIfValue(reserr, 'message')) {
            errMsg = reserr.message;
          } else {
            errMsg = reserr.statusText;
          }
        } catch (locerror) {
          // TODO: Log these type of errors to server. TODO: Create server logger.
          // errMsg = locerror.toString();
          errMsg = reserr.statusText;
          console.error('API: + LOCAL Error:', reserr, locerror);
        }
      } else {
        errMsg = 'You appear to be offline. Connect to the internet and try again.';
        // alert(errMsg);
        this.dialog.open(WasUp, {
          width: '300px', data: { title: 'Attention', icon: 'report_problem', body: errMsg, stayopen: false }
        });
      }
    }
    // this.dialog.open(WasUp, {
    //   width: '300px', data: { title: reserr.statusText, icon: 'report_problem', body: errMsg, stayopen: false }
    // });
    return throwError(() => {
      const error: any = new Error(errMsg);
      error.name = reserr.statusText;
      error.stack = reserr.error;
      return error;
    });
  }
  private extractData(res: any) {
    // console.log('extractData', res);
    const body = res.data;
    // Add http status to body //
    body.status = res.http_status;
    // if status === 202, then show the res.refresh_msg
    if (body.status === 202) {
      console.log('New version available! Click OK to update.');
      this.confirmDialog(res.refresh_msg).then((_isConfirmed) => {
        if (_isConfirmed) {
          console.warn('extractData, new version reload');
          location.reload();
        } else {
          try {
            // NOTE: all user APIS can return a `special_message`
            if (body.hasOwnProperty('special_message')) {
              // console.log(`ApiConnectionService: extractData: special_message:[${body.special_message}]`);
              this.dialog.open(WasAlert, {
                data: { title: body.special_message.title, body: body.special_message.message }
              });
            }
          } catch (extractError) {
            console.error('extractData', extractError);
          }
        }
      });
    }
    return body;  // as User || { };
  }
  confirmDialog(message: string) {
    return new Promise<boolean>(resolve => {
      this.dialog.open(WasAlert, {
        data: { title: 'Attention', body: message, style: 'WasAlertStyleConfirm' }
      }).afterClosed().subscribe(result => {
        // result is the index of the button pressed
        if (result === 1) {
          return resolve(true);
        } else {
          return resolve(false);
        }
      });
    });
  }

  // AUTHENTICATION //
  private setSession(authResult: AuthResult) {
    const expiresAt = +new Date(authResult.expiresIn * 1000);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt));
  }

  public isLoggedIn() {
    if (Date.now() > this.getExpiration()) {
      return false
    } else {
      return true;
    }
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_at");
    if (expiration) {
      return JSON.parse(expiration) as number;
    } else {
      return 0;
    }
  }


  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
  }

  /**
   * Logs in a user with username and password and returns the JWT token.
   *
   * @example
   * this.api.login('pat@example.com', 'password').subscribe({
   *  next: (token: AuthResult) => {},
   *  error: (err: Error) => {}
   * }):
   *
   *
   * @param email The login email
   * @param password The login password
   * @returns {Observable<AuthResult>} The auth result
   */
  login(email: string, password: string): Observable<AuthResult> {
    const _headers = this.getHeaders(email, password);
    return this.http.post(this.loginUrl, { email: email, password: password }, { headers: _headers, withCredentials: true }).pipe(
      map((res: any) => {
        if (res.http_status >= 200 && res.http_status < 300) {
          this.setSession(res.data);
        } else {
          console.error('login', res);
        }
        return res.data as AuthResult;
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }
  // AUTHENTICATION //


  getProfile(): Observable<UserModel> {
    const _headers = this.getHeaders();
    return this.http.get(this.profileUrl, { headers: _headers, withCredentials: true }).pipe(
      map((res: any) => {
        return res.data as UserModel;
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }

  updateProfile(userParams: UserParams): Observable<UserModel> {
    const _headers = this.getHeaders();
    return this.http.post(this.profileUrl, userParams, { headers: _headers, withCredentials: true }).pipe(
      map((res: any) => {
        return res.data as UserModel;
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }

  // ACCOUNT CREATION //
  /**
   * Sends an email to the user with a verification token.
   *
   * @param {string} email The email address to send the verification token to.
   * @returns http_status: 200 if successful.
   */
  tokenPerson(email: string): Observable<any> {
    const _headers = this.getHeaders();
    // NOTE: Use share to avoid duplicate calls
    return this.http.post(this.emailVerificationTokenUrl, { email: email },
      { headers: _headers, withCredentials: true }).pipe(
        map(this.extractData),
        catchError(err => {
          return this.handleError(err);
        }), share());
  }
  // Verify the recovery token and login
  /**
   *
   * @param {string} email The email address that received the verification token.
   * @param {string} password The password to set for the user.
   * @param {string} verification_token The verification token sent to the user.
   * @returns http_status: 201 if account was created, otherwise 406 if the token is invalid or expired.
   */
  verifyPerson(email: string, password: string, verification_token: string): Observable<any> {
    const _headers = this.getHeaders(email, password);
    // NOTE: Use share to avoid duplicate calls
    return this.http.post(this.emailVerificationUrl,
      { email: email, password: password, verification_token: verification_token }, { headers: _headers, withCredentials: true }).pipe(
        map((res: any) => {
          if (res.http_status >= 200 && res.http_status < 300) {
            this.setSession(res.data);
          } else {
            console.error('login', res);
          }
          return res.data as AuthResult;
        }), catchError(err => {
          return this.handleError(err);
        }), share());
  }

  /**
   * Verifies the email address using the token that the user has received in the email.
   *
   * @param {string} email The email address to verify
   * @param {string} verification_token The token received in the email
   * @returns http_status: 200 if the token is valid, otherwise 406 on incorrect or expired token.
   */
  verifyTokenSimple(email: string, verification_token: string): Observable<any> {
    const _headers = this.getHeaders();
    // NOTE: Use share to avoid duplicate calls
    return this.http.post(this.simpleEmailVerificationUrl,
      { email: email, verification_token: verification_token }, { headers: _headers, withCredentials: true }).pipe(
        map((res: any) => {
          return this.extractData(res);
        }), catchError(err => {
          return this.handleError(err);
        }), share());
  }
  // ACCOUNT CREATION //


  /**
  * Set or change a password of a user.
  *
  * @param {string} user_id string: The user id.
  * @param {string} password string: The raw password.
  * @param {string} new_password string: OPTIONAL The new password on password changes.
  * @returns Success or failure status code and message.
  */
  authPerson(user_id: string, password: string, new_password?: string): Observable<any> {
    const _headers = this.getHeaders();
    let apiobject;
    if (new_password) {
      apiobject = { user_id: user_id, new_password: new_password };
    } else {
      apiobject = { user_id: user_id };
    }
    // NOTE: Use share to avoid duplicate calls
    return this.http.post(this.changePasswordUrl, apiobject, { headers: _headers, withCredentials: true }).pipe(
      map((res: any) => {
        return this.extractData(res);
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }



  // NOTE: Observable<JSON> Allows a generic return of some json
  /**
   * Returns a list of song titles that exist in the database
   *
   * @param {string} snippet If snippet is passed in then the autocomplete list will be filtered by it.
   * @returns {Observable<string[]>}
   */
  getAutocomplete(snippet?: string): Observable<string[]> {
    const _headers = this.getHeaders();
    let apiParams
    if (snippet) {
      apiParams = { snippet: snippet }
    }
    else {
      apiParams = {}
    }
    return this.http.get(this.autocompleteUrl, { params: apiParams, headers: _headers }).pipe(
      map((res: any) => {
        return this.extractData(res).autocomplete;
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }

  /**
   * Returns a song if it exists in the database
   *
   * @param {string} id Returns the song where that has this title (id).
   * @returns {Observable<LCCSong | null>}
   */
  getSong(id: string): Observable<LCCSong | null> {
    const _headers = this.getHeaders();
    return this.http.get(this.getSongUrl, { params: { id: id }, headers: _headers }).pipe(
      map((res: any) => {
        return this.extractData(res);
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }

  /**
   * Returns a ppt if it exists in the database
   *
   * @param {string} id Returns the ppt where that with this id.
   * @returns {Observable<LCCPPT | null>}
   */
  getPPT(id: string): Observable<LCCPPT | null> {
    const _headers = this.getHeaders();
    return this.http.get(this.getPPTUrl, { params: { id: id }, headers: _headers }).pipe(
      map((res: any) => {
        return this.extractData(res);
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }


  /**
   * Saves a ppt to the database
   *
   * @param {LCCPPT} lccppt
   * @returns {Observable<HttpResponse<Blob>>} Returns 201 if new ppt was created, 200 if old one was updated, or a 400 if an error was encountered.
   */
  setPPT(lccppt: LCCPPT): Observable<HttpResponse<Blob>> {
    const _headers = this.getHeaders();
    return this.http.post(this.setPPTUrl, lccppt, { observe: 'response', responseType: 'blob', headers: _headers }).pipe(
      map((res: any) => {
        return res;
      }), catchError(err => {
        return this.handleError(err);
      }), share());
  }

}
