import { Component, OnInit, Input, Output, EventEmitter, Inject, ChangeDetectorRef } from '@angular/core';
import { UntypedFormControl, UntypedFormBuilder, UntypedFormGroup, Validators, UntypedFormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { ApiConnectionService } from '../api-connection.service';
import { WasAlert } from '../popover/wasalert/wasalert.dialog';
import { WasUp } from '../popover/wasup/wasup.dialog';
import { LocalStorageService } from '.././local-storage.service';
import { LCCSection } from '../structs';

@Component({
  selector: 'lct-ppt',
  templateUrl: './ppt.component.html',
  styleUrls: ['./ppt.component.css']
})
export class PptComponent implements OnInit {
  @Input() public autocomplete: string[] = [];

  // This component will have ppt form group which contains an id and an array of sections
  // Each section will have an id, text, and options
  // Each option will have a prependBlank, appendBlank, and isNew
  // This component will display a form and embed the section.component
  sectionForm = this.getNewBlankSection();
  pptForm = this.fb.group({
    id: ['', [Validators.required, Validators.maxLength(64)]],
    email: ['', Validators.email],
    sections: this.fb.array([
      this.sectionForm
    ])
  });
  backgroundColor = '#2e1100';  // (46, 17, 0, 1)
  textColor = '#fffed0';  // (255, 254, 208, 1)



  constructor(
    public api: ApiConnectionService, @Inject(UntypedFormBuilder) private fb: UntypedFormBuilder,
    public dialog: MatDialog, public localStorageService: LocalStorageService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.loadLastPPT();
    this.pptForm.valueChanges.subscribe(val => {
      // console.log(val);  // NOTE: val is this.pptForm.value
      this.localStorageService.set('pptForm', val);
    })
  }

  onColorPickerChangeBackground(event: Event) {
    this.backgroundColor = (event.target as HTMLInputElement).value;
    console.log('Color picker value changed:', this.backgroundColor);
    // Do something with the new color value
  }
  onColorPickerChangeFont(event: Event) {
    this.textColor = (event.target as HTMLInputElement).value;
    console.log('Color picker value changed:', this.textColor);
  }

  loadLastPPT() {
    this.localStorageService.get('pptForm')
      .then((value: any): void => {
        if (typeof value !== 'undefined' && value !== undefined) {
          // this.pptForm.patchValue(value);
          this.pptForm.reset({
            id: value.id,
            email: value.email
          });
          this.sections.clear();
          if (value.sections) {
            value.sections.forEach((section: LCCSection) => {
              this.sections.push(this.getLoadedSectionForm(section));
            });
            // Force a change detection cycle
            this.cdr.detectChanges();
          }
        } else {
        }
      }
      ).catch(this.handleError);
  }

  getLoadedSectionForm(_section: LCCSection) {
    let newSectionForm = this.getNewBlankSection();
    newSectionForm.reset({
      id: _section.id,
      text: _section.text,
      options: {
        prependBlank: _section.options.prependBlank,
        appendBlank: _section.options.appendBlank,
        isNew: _section.options.isNew,
        addLicense: _section.options.addLicense,
        backgroundColor: _section.options.backgroundColor,
        textColor: _section.options.textColor
      }
    });
    return newSectionForm;
  }

  private handleError(error: any): Promise<any> {
    // .catch(this.handleError);
    console.error('ppt.component: An error occurred', error);  // for demo purposes
    return Promise.reject(error.message || error);
  }

  resetForm() {
    this.pptForm.reset();
    this.sections.clear();
    this.sections.push(this.getNewBlankSection());
  }


  get emailFormCtrl() {
    return this.pptForm.get('email') as UntypedFormControl;
  }

  // This function returns the section form array
  get sections(): UntypedFormArray {
    // return this.pptForm.controls['sections'] as FormArray;
    return this.pptForm.get('sections') as UntypedFormArray;
  }

  getErrorMessage() {
    if (this.emailFormCtrl.hasError('required')) {
      return 'You must enter a value';
    }

    return this.emailFormCtrl.hasError('email') ? 'Not a valid email' : '';
  }

  getSectionForm(_index: number) {
    return this.sections.at(_index) as UntypedFormGroup;
  }

  // This function returns a blank section form
  // TODO: Save current working object to localstorage and restore it on reload, then add a clear button to start a new ppt.
  getNewBlankSection() {
    return this.fb.group({
      id: ['', [Validators.required, Validators.maxLength(64)]],
      text: ['', Validators.required],
      options: this.fb.group({
        prependBlank: [false],
        appendBlank: [true],
        isNew: [false],
        addLicense: [true],
        backgroundColor: this.backgroundColor,
        textColor: this.textColor
      })
    });
  }

  // This function adds a new section to the form array
  addSection() {
    // this.aliases.push(this.fb.control(''));
    this.sections.push(this.getNewBlankSection());
  }

  removeSection(_index: number) {
    console.log("removeSection", _index);
    this.sections.removeAt(_index);
  }

  drop(event: CdkDragDrop<any[]>) {
    // https://phamhuuhien.medium.com/how-to-create-drag-and-drop-accordion-list-in-angular-b75dd004bb4e
    // console.log('drop', event);
    moveItemInArray(this.pptForm.value.sections, event.previousIndex, event.currentIndex);
    moveItemInArray(this.sections.controls, event.previousIndex, event.currentIndex);
  }

  // This function accepts the section array and calls an api to create a new ppt
  createPPT() {
    // TODO: Save if section was open or shut [expanded]="true" in section.component.html
    console.log('createPPT', this.pptForm.value);
    // Force into allowed filename characters and append .pptx to the filename
    const pptFileName = this.pptForm.value.id.replace(/\s/g, '_').replace(/[^a-zA-Z0-9_]/g, '_') + '.pptx';
    const loadingdialogRef = this.dialog.open(WasUp, {
      width: '350px', data: {
        title: `Creating Powerpoint ${pptFileName}`,
        icon: 'spinner', body: 'Creating...', stayopen: true
      }
    });
    this.api.setPPT(this.pptForm.value).subscribe({
      next: (_evt) => {
        console.log('console', _evt);
        if (_evt.body) {
          // console.log(_evt.headers.get('Content-Disposition'));
          // https://stackoverflow.com/questions/57326028/angular-how-to-download-file-from-web-api-on-subscribe
          // https://stackoverflow.com/questions/51779498/return-file-from-web-api-in-angular-5
          // Could also use FileSaver: https://github.com/eligrey/FileSaver.js as shown https://stackoverflow.com/a/59678353/4055875
          const blob = new Blob([_evt.body], { type: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' });
          const url = window.URL.createObjectURL(blob);
          // NOTE: This commented out version works, but it doesn't name the file correctly
          // const pwa = window.open(url);
          // if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
          //   alert('Please disable your Pop-up blocker and try again.');
          // }
          // Create a link to download the file
          let a = document.createElement("a");
          a.href = url;
          this.pptForm.value.id;
          a.download = pptFileName;
          document.body.appendChild(a);
          a.click();
          // Not sure if this is needed
          window.URL.revokeObjectURL(url);
          loadingdialogRef.close();
        }
      },
      error: (err: Error) => {
        loadingdialogRef.close(false);
        // console.error('api: getSong: error: ' + err);
        this.dialog.open(WasAlert, { data: { title: 'Attention', icon: 'warning', body: 'Error creating powerpoint.', style: 'WasAlertStyleWarning' } });
      },
      complete: () => {
        console.log('api: createPPT: Observer got a complete notification');
      }
    });
  }

}
