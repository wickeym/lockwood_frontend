import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError, of, tap } from 'rxjs';
import { share, catchError, map, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserService } from './user.service';

// @Injectable()
// export class CustomInterceptor implements HttpInterceptor {

//   constructor() { }

//   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

//     console.log("outgoing request", request);
//     // request = request.clone({ withCredentials: true });
//     // console.log("new outgoing request", request);

//     return next
//       .handle(request).pipe(
//         tap((ev: HttpEvent<any>) => {
//           console.log("got an event", ev)
//           if (ev instanceof HttpResponse) {
//             console.log('event of type response', ev);
//           }
//         })
//       );
//   }
// }

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, public userService: UserService) { }
  // https://angular.io/guide/http#intercepting-requests-and-responses
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const idToken = localStorage.getItem("id_token");
    let handled: boolean = false;
    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + idToken)
      });
      // TODO: https://careydevelopment.us/blog/angular-how-to-handle-errors-with-an-http-interceptor
      // NOTE: Can pass retry(_cnt) into pipe to retry the request
      return next.handle(cloned).pipe(
        catchError((returnedError) => {
          let errorMessage = '';

          if (returnedError.error instanceof ErrorEvent) {
            errorMessage = `Error: ${returnedError.error.message}`;
          } else if (returnedError instanceof HttpErrorResponse) {
            errorMessage = `Error Status ${returnedError.status}: ${returnedError.error.error} - ${returnedError.error.message}`;
            handled = this.handleServerSideError(returnedError);
          }

          console.error('api-interceptor: catchError', errorMessage, returnedError);

          if (!handled) {
            if (errorMessage !== '') {
              return throwError(() => {
                const error: any = new Error(errorMessage);
                return error;
              });
            } else {
              return throwError(() => {
                const error: any = new Error("Unexpected problem occurred");
                return error;
              });
            }
          } else {
            return of(returnedError);
          }
        })
      );
    }
    else {
      return next.handle(req);
    }
  }

  private handleServerSideError(error: HttpErrorResponse): boolean {
    let handled: boolean = false;

    switch (error.status) {
      case 401:
        this.router.navigate(['login']);
        // this.routeMessageService.message = "Please login again.";
        // this.authenticationService.logout();
        this.userService.logout();
        handled = true;
        break;
      case 403:
        // this.routeMessageService.message = "Please login again.";
        // this.authenticationService.logout();
        // this.router.navigate(['login']);
        this.userService.logout();
        handled = true;
        break;
    }

    return handled;
  }
}
