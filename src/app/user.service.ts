// import * as Raven from 'raven-js';
import { Injectable, NgZone, Directive } from '@angular/core';
import { ApiConnectionService } from './api-connection.service';
import { LocalStorageService } from './local-storage.service';
import { of as observableOf, from, Observable, ReplaySubject } from 'rxjs';
import { map, mergeMap, catchError, share } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';  // MatDialogRef, MAT_DIALOG_DATA
import { WasLogin } from './popover/waslogin/waslogin.dialog';
import { LctCreate } from './popover/lctcreate/lctcreate.dialog';
import { WasUp } from './popover/wasup/wasup.dialog';
import { WasAlert } from './popover/wasalert/wasalert.dialog';
// import { WasProfile } from './ui/popover/wasprofile/wasprofile.dialog';
// export * from './structs';
import { UserModel, AuthResult, UserParams } from './structs';

// Following the example given here:
// http://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/
// Full code: https://github.com/jhades/angular2-rxjs-observable-data-services
// Thanks @jhades

/**
 * The UserService.
 *
 * ```typescript
 * // Import in any component this is to be used:
 * import { UserService } from 'WickeyAppStore';
 *
 * // Inject it in the constructor
 * constructor(userService: UserService) { }
 *
 * // Get user:
 * this.userService.user().subscribe();
 * ```
 */
@Directive()
@Injectable({
  providedIn: 'root'
})
export class UserService {
  // Add get profile function
  // Add isLoggedIn function
  // Add login function
  // Add logout function
  // Add register function
  // Add forgot password function
  // Add reset password function
  // Add update password function
  // Add update profile function
  // Add delete profile function

  // The user will be an observable.
  /**
 * @property
 * @ignore
 */
  private readonly merchantID = '1105404'; // your BlueSnap Merchant ID;
  private _user: ReplaySubject<UserModel> = new ReplaySubject(1);
  private _loginChange: ReplaySubject<boolean> = new ReplaySubject(1);
  private _onAccountCreate: ReplaySubject<boolean> = new ReplaySubject(1);
  // private _favorites: ReplaySubject<AppGroup> = new ReplaySubject(1);
  // private _favoritesObj: AppGroup;
  // private _is_favorite: ReplaySubject<Boolean> = new ReplaySubject(1);
  // private _is_favoriteObj: Boolean = false;
  private _userObj: UserModel;
  private _createNewUser = false;
  private _isLoggedIn = false;
  private _loaded = false;
  private standalone: boolean = false;
  private lut: string[] = [];
  /**@ignore*/
  constructor(
    private apiConnectionService: ApiConnectionService,
    private localStorageService: LocalStorageService,
    private _ngZone: NgZone,
    public dialog: MatDialog
  ) {
    // Set to an anonymous user
    this._userObj = new UserModel();
    this._isLoggedIn = this.apiConnectionService.isLoggedIn();
    this._loginChange.next(this._isLoggedIn);
    this.loadUser();
    // Track login status
    this._user.subscribe((usr: UserModel) => {
      if (this.apiConnectionService.isLoggedIn()) { // Logged in
        if (this._isLoggedIn === false) { // Check if status changed
          console.warn('UserService LOGGED IN');
          this._isLoggedIn = true;
          this._loaded = true;
          this._loginChange.next(this._isLoggedIn);
        } else if (this._loaded === false) {  // Ensure initial load update
          this._loaded = true;
          this._loginChange.next(this._isLoggedIn);
        }
      } else { // Logged out
        if (this._isLoggedIn === true) { // Check if status changed
          console.warn('UserService LOGGED OUT');
          this._isLoggedIn = false;
          this._loaded = true;
          this._loginChange.next(this._isLoggedIn);
        } else if (this._loaded === false) { // Ensure initial load update
          this._loaded = true;
          this._loginChange.next(this._isLoggedIn);
        }
      }
    });
    this._loginChange.subscribe((_loggedIn: boolean) => {
      // Load inapps on all login changes (also ensures user object exists)
      // this.loadInapps();
      // this.loadFreebieSettings();
      // this.loadIfFavorite();
      // this.loadNewsfeed();
    });
  }


  /**
  *  // test if the string is empty or null
  * @ignore
  */
  isEmpty(str: string): boolean {
    return (!str || 0 === str.length);
  }
  /**
   * Pushes boolean to all subscribers on login status changes and also on initial load.
   * So this can be used to monitor login status changes to route url, or to check login status on user load.
   *
   * @example
   * Use in angular template with the `async` pipe
   * userService.loginChange | async
   * Subscribe in ts: userService.loginChange.subscribe
   *
   * @readonly
   */
  get loginChange() {
    return this._loginChange;
  }
  get onAccountCreate() {
    return this._onAccountCreate;
  }
  /**
   * Returns true if user is logged in and else false.
   * NOTE: This should only be used if UserService has already loaded the User, otherwise the result is un-reliable (Check out loginChange).
   */
  get isLoggedInVal() {
    return this.apiConnectionService.isLoggedIn();
  }
  get isLoggedIn() {
    return this._user.pipe(map((usr: UserModel) => {
      return this.apiConnectionService.isLoggedIn();
    }));
  }

  /**
   * Pushes User Object to all subscribers on every user update.
   *
   * @example
   * Use in angular template with the `async` pipe
   * userService.user | async
   * Subscribe in ts: userService.user.subscribe
   *
   * @readonly
   */
  get user() {
    return this._user;
  }
  get userObject() {
    return this._userObj;
  }
  get isLoaded() {
    return this._loaded;
  }
  // get favorites() {
  //   return this._favorites;
  // }
  // get favoritesObject() {
  //   return this._favoritesObj;
  // }
  // get isFavorite() {
  //   return this._is_favorite;
  // }
  // get isFavoriteObject() {
  //   return this._is_favoriteObj;
  // }


  private pushSubscribers(_usr: UserModel) {
    // TODO: Only push if object values changed. This does not work, need to store a copy of last, else it simply sets a reference,
    //  thus both are updated.
    const keys = Object.keys(_usr);
    let foundChange = false;
    for (const key of keys) {
      let kk = key as keyof UserModel
      if (this._userObj[kk] !== _usr[kk]) {
        foundChange = true;
        break;
      }
    }
    if (foundChange) {
      console.log('pushSubscribers:CHANGES');
      // this._user.next(_usr);
    }
    this._userObj = _usr;
    this._user.next(_usr);
    // NOTE: This is only used with Async to push the last value: this._user.complete();
  }
  private saveLocal(_key: string, _obj: any) {
    this.localStorageService.set(_key, _obj);
  }
  /** @ignore */
  loadUser() {
    this.localStorageService.get('user')
      .then((value: any): void => {
        if (typeof value !== 'undefined' && value.user_id !== undefined) {
          const _localUser = value as UserModel;
          this.pushSubscribers(_localUser);
          // show the Account Create iff account was being created in the last session
          if (_localUser.logging_in) {
            console.log('UserService: User was logging in last session');
            // this.dialog.open(LctCreate, {
            //   data: { email: _localUser.email }
            // });
          }
          // normal load
          // console.log('UserService loadUser: load user from db', _localUser);
          this._createNewUser = false;
          if (this.apiConnectionService.isLoggedIn()) {
            this.updateUser({});
          }
        } else {
          // Load the anonymous user.
          this.pushSubscribers(this._userObj);
          // this._onAccountCreate.next(true);
          // // create new user
          // this._createNewUser = true;
          // this.updateUser({});
          if (this.apiConnectionService.isLoggedIn()) {
            this.updateUser({});
          }
        }
      }
      ).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    // .catch(this.handleError);
    console.error('UserService: An error occurred', error);  // for demo purposes
    return Promise.reject(error.message || error);
  }
  /**
   * @ignore
   */
  checkStandalone(): void {
    // THIS ONLY WORKS ON iOS
    if ('standalone' in window.navigator) {
      // https://developer.mozilla.org/en-US/docs/Web/API/Navigator
      this.standalone = (<any>window.navigator).standalone;
      console.log('homescreen iOS', this.standalone);
    } else {
      // https://developers.google.com/web/updates/2015/10/display-mode
      if (window.matchMedia('(display-mode: standalone)').matches) {
        this.standalone = true;
      } else {
        this.standalone = false;
      }
      console.log('homescreen chrome', this.standalone);
    }
  }

  /**
   * Fast UUID generator, RFC4122 version 4 compliant.
   * @author Jeff Ward (jcward.com).
   * @license MIT license
   * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
   * @ignore
   **/
  guid(): string {
    /* tslint:disable: no-bitwise */
    for (let i = 0; i < 256; i++) {
      this.lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
    }
    let d0;
    let d1;
    let d2;
    let d3;
    if (typeof (window) !== 'undefined' && typeof (window.crypto) !== 'undefined'
      && typeof (window.crypto.getRandomValues) !== 'undefined') {
      const dvals = new Uint32Array(4);
      window.crypto.getRandomValues(dvals);
      d0 = dvals[0];
      d1 = dvals[1];
      d2 = dvals[2];
      d3 = dvals[3];
    } else {
      d0 = Math.random() * 0xffffffff | 0;
      d1 = Math.random() * 0xffffffff | 0;
      d2 = Math.random() * 0xffffffff | 0;
      d3 = Math.random() * 0xffffffff | 0;
    }
    return this.lut[d0 & 0xff] + this.lut[d0 >> 8 & 0xff] + this.lut[d0 >> 16 & 0xff] + this.lut[d0 >> 24 & 0xff] + '-' +
      this.lut[d1 & 0xff] + this.lut[d1 >> 8 & 0xff] + '-' + this.lut[d1 >> 16 & 0x0f | 0x40] + this.lut[d1 >> 24 & 0xff] + '-' +
      this.lut[d2 & 0x3f | 0x80] + this.lut[d2 >> 8 & 0xff] + '-' + this.lut[d2 >> 16 & 0xff] + this.lut[d2 >> 24 & 0xff] +
      this.lut[d3 & 0xff] + this.lut[d3 >> 8 & 0xff] + this.lut[d3 >> 16 & 0xff] + this.lut[d3 >> 24 & 0xff];
  }
  /**
   * @ignore
   */
  checkIfValue(_obj: any, _key: string): boolean {
    let hasValue = false;
    if (_obj.hasOwnProperty(_key)) {
      if (_obj[_key] !== undefined && _obj[_key] !== null) {
        hasValue = true;
      }
    }
    return hasValue;
  }

  /**
   * Update User object, saves locally and to server (Must be logged in).
   * Use the lctcreate to first create a new user (sendToken, verifyToken).
   *
   * @param [userParams] OPTIONAL object of parameters to update user.
   * @ignore
   */
  updateUser(userParams: UserParams): Observable<UserModel> {
    if (this._createNewUser === true) {
      this._createNewUser = false;
      // NOTE: Check if user_id is in a cookie, to share user_id accross all wickeyappstore apps
      const _cookie_userid = this.localStorageService.cookie_read('user_id');
      if (_cookie_userid !== null && _cookie_userid !== '') {
        console.warn('UserService: FOUND USER ID IN COOKIE');
        userParams.user_id = _cookie_userid;
      } else {
        console.warn('UserService: NO USER, CREATE USER');
        userParams.user_id = this.guid();
        userParams.new_account = true;
      }
    } else {
      if (this._userObj.account_verified === false && this._userObj.coins === 0) {
        // user exists, but is still anonymous and does not have any data saved
        const _cookie_userid = this.localStorageService.cookie_read('user_id');
        if (_cookie_userid !== null && _cookie_userid !== '') {
          console.warn('UserService2: FOUND USER ID IN COOKIE');
          userParams.user_id = _cookie_userid;
        }
      }
    }
    // console.log('============UserService updateUser=========', this._userObj);
    // NOTE: Set params to current user, then update to sent in userParams, if any exist
    if (!this.checkIfValue(userParams, 'version')) {
      userParams.version = .1;
    }
    if (!this.checkIfValue(userParams, 'lang')) {
      userParams.lang = 'english';
    }
    if (!this.checkIfValue(userParams, 'freebie_used')) {
      userParams.freebie_used = this._userObj.freebie_used;
    }
    if (!this.checkIfValue(userParams, 'rated_app')) {
      userParams.rated_app = this._userObj.rated_app;
    }
    if (!this.checkIfValue(userParams, 'push_id')) {
      userParams.push_id = this._userObj.push_id;
    }
    // GET IF LAUNCHED FROM HOMESCREEN //
    this.checkStandalone();
    if (this.standalone) {
      userParams.standalone = this.standalone;
    }
    const _obs = this.apiConnectionService.updateProfile(userParams);
    _obs.subscribe({
      next: (res) => {
        if (res.status === 201) {
          // On new user/recover
          // UPDATE USER //
          this.localStorageService.cookie_write_multi('user_id', res.user_id);
          this.pushSubscribers(res);
          this.saveLocal('user', res);
        } else {
          // NOTE: If a user has an email, the account was either verified by token or doesn't belong to someone else.
          if (res.email && res.user_id) {
            this._userObj.user_id = res.user_id;
            this.localStorageService.cookie_write_multi('user_id', res.user_id);
          }
          if (this.checkIfValue(res, 'email')) {
            this._userObj.email = res.email;
          }
          if (this.checkIfValue(res, 'bs_id')) {
            this._userObj.bs_id = res.bs_id;
          }
          if (this.checkIfValue(res, 'coins')) {
            this._userObj.coins = res.coins;
          }
          if (this.checkIfValue(res, 'rated_app')) {
            this._userObj.rated_app = res.rated_app;
          }
          if (this.checkIfValue(res, 'push_id')) {
            this._userObj.push_id = res.push_id;
          }
          if (this.checkIfValue(res, 'account_verified')) {
            this._userObj.account_verified = res.account_verified;
          }
          this._userObj.created_time = res.created_time;
          this._userObj.freebie_used = res.freebie_used;
          this.pushSubscribers(this._userObj);
          this.saveLocal('user', this._userObj);
        }
        // Add user context in sentry
        // Raven.setUserContext({id: res.user_id});
      },
      error: (error) => {
        console.error('UserService: Error updating user', error);
      },
      complete: () => {
        // console.log('UserService: User updated');
      }
    });
    return _obs;
  }


  /**
   * Get user profile.
   *
   * @param email The user's email address.
   * @param password The password.
   * @ignore
   */
  profile(): Observable<UserModel> {
    const _obs = this.apiConnectionService.getProfile();
    _obs.subscribe({
      next: (usr: UserModel) => {
        this._userObj = usr;
        this._userObj.is_anonymous = false
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
      },
      error: (error: any) => {
        console.log(`UserService: login: error:`, error);
        // NOTE: Handle errors in calling component.
        // alert(`Attention!\n${error}`);
        // <any>error | this casts error to be any
      }
    })
    return _obs;
  }

  /**
   * Update the user's push_id only if this id is different than the currenly saved id.
   *
   * @param push_id The OneSignal user_id (push_id)
   * @ignore
   */
  updateUserPushId(push_id: string) {
    if (this._userObj.push_id !== push_id) {
      // console.log('UserService updateUserPushId: CHANGE:', push_id);
      this.updateUser({ push_id: push_id }).subscribe({
        next: (usr) => {
          // console.log('UserService updateUserPushId: RETURN:', usr);
        },
        error: (error) => {
          console.log('UserService updateUserPushId: RETURN ERROR:', error);
          // NOTE: Handle errors in calling component.
          // alert(`Attention!\n${error}`);
          // <any>error | this casts error to be any
        },
        complete: () => { }
      });
    } else {
      // console.log('UserService updateUserPushId: NO CHANGE:', push_id);
    }
  }

  /**
   * Login the user.
   *
   * @param email The user's email address.
   * @param password The password.
   * @ignore
   */
  login(email: string, password: string): Observable<AuthResult> {
    const loadingdialogRef = this.dialog.open(WasUp, {
      width: '300px', data: { title: 'Logging in', icon: 'spinner', body: 'Updating...', stayopen: true }
    });
    const _obs = this.apiConnectionService.login(email, password);
    _obs.subscribe({
      next: (usr: AuthResult) => {
        loadingdialogRef.close();
        this._userObj = usr.user;
        this._userObj.is_anonymous = false;
        this.localStorageService.cookie_write_multi('user_id', this._userObj.user_id);
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
      },
      error: (error: any) => {
        console.log(`UserService: login: error:`, error);
        loadingdialogRef.close();
        // NOTE: Handle errors in calling component.
        // alert(`Attention!\n${error}`);
        // <any>error | this casts error to be any
      }
    })
    return _obs;
  }

  // New way to do observables https://rxjs.dev/deprecations/subscribe-arguments
  /**
   * Add/Update a user's pin/pass code used in faster or more secure logins.
   *
   * @param password The password, or current password if updating.
   * @param new_password OPTIONAL: New password.
   * @ignore
   */
  setPassword(password: string, new_password?: string): Observable<any> {
    const _obs = this.apiConnectionService.authPerson(this._userObj.user_id, password, new_password);
    _obs.subscribe({
      next: (res) => {
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
      },
      error: (error) => {
        // <any>error | this casts error to be any
        // NOTE: Handle errors in calling component.
      }
    });
    return _obs;
  }

  /**
   * Log user out, this deletes all local storage and cookies.
   * @ignore
  */
  logout() {
    console.log('UserService:logOut');
    this.apiConnectionService.logout();
    // Delete user from local storage
    this.localStorageService.delete('user');
    // Delete user_id cookie
    this.localStorageService.cookie_remove('user_id');
    this.localStorageService.cookie_remove_multi('user_id');
    this._isLoggedIn = this.apiConnectionService.isLoggedIn();
    this._loginChange.next(this._isLoggedIn);
    // Set/Reload an anonymous user
    this._userObj = new UserModel();
    this.loadUser();
  }

  /**
   * Step 1 of email verification.
   * Sends token to email, to begin email verification process.
   *
   * @param userParams {token_email: string}
   */
  sendToken(email: string): Observable<any> {
    this._userObj.email = email;
    this.pushSubscribers(this._userObj);
    this.saveLocal('user', this._userObj);
    const _obs = this.apiConnectionService.tokenPerson(email);
    _obs.subscribe({
      next: (res) => {
        this._userObj.logging_in = true;
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
      },
      error: (error) => {
        // <any>error | this casts error to be any
        // NOTE: Handle errors in calling component.
      }
    });
    return _obs;
  }
  /**
   * This will stop the token/account creation process.
   */
  stopToken() {
    this._userObj.logging_in = false;
    this.pushSubscribers(this._userObj);
    this.saveLocal('user', this._userObj);
  }

  /**
   * Step 2 of email verification/account creation.
   * Send token that was sent via email to complete email verification and create the new account.
   *
   * @param userParams {token_email: string, token: string}
   * @returns {Observable<AuthResult>}
   */
  verifyToken(email: string, password: string, token: string): Observable<AuthResult> {
    const _obs = this.apiConnectionService.verifyPerson(email, password, token);
    _obs.subscribe({
      next: (usr: AuthResult) => {
        // Set logging in process off //
        this._userObj = usr.user;
        this._userObj.is_anonymous = false;
        this.localStorageService.cookie_write_multi('user_id', this._userObj.user_id);
        // UPDATE USER //
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
        // NOTE: account_verified should be true now.
      },
      error: (error) => {
        // Set logging in process off //
        this._userObj.logging_in = false;
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
        // <any>error | this casts error to be any
        // NOTE: Handle errors in calling component.
      }
    });
    return _obs;
  }

  /**
   * Step 2 of email verification.
   * Send token that was sent via email to complete email verification.
   *
   * @param userParams {token_email: string, token: string}
   * @ignore
   */
  verifyTokenSimple(email: string, token: string): Observable<any> {
    const _obs = this.apiConnectionService.verifyTokenSimple(email, token);
    _obs.subscribe({
      next: (usr: AuthResult) => {
        // Set logging in process off //
        this._userObj = usr.user;
        this._userObj.is_anonymous = false;
        this.localStorageService.cookie_write_multi('user_id', this._userObj.user_id);
        // UPDATE USER //
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
        // NOTE: account_verified should be true now.
      },
      error: (error) => {
        // Set logging in process off //
        this._userObj.logging_in = false;
        this.pushSubscribers(this._userObj);
        this.saveLocal('user', this._userObj);
        // <any>error | this casts error to be any
        // NOTE: Handle errors in calling component.
      }
    });
    return _obs;
  }

  /** @ignore */
  _openLogin(): Observable<any> {
    let _obs;
    // if (this._isLoggedIn) {
    //   _obs = this.dialog.open(WasAlert, {
    //     data: { title: 'Do you wish to log out?', body: '', style: 'WasAlertStyleWarning' }
    //   }).afterClosed().pipe(map(retVal => retVal), mergeMap(result => {
    //     if (result === 1) {
    //       console.log('log out this user');
    //       this.logout();
    //       return observableOf(null);
    //     } else {
    //       return observableOf(null);
    //     }
    //   }), share());
    // } else {
    //   _obs = this.dialog.open(WasLogin, { data: {} }).afterClosed();
    // }
    _obs = this.dialog.open(WasLogin, { data: {} }).afterClosed();
    return _obs;
  }
  /**
   * Open Login if not logged in, else confirm logout.
  */
  openLogin(): Observable<any> {
    const _obs = this._openLogin();
    _obs.subscribe(_ret => { });
    return _obs;
  }
  /** @ignore */
  _openAccountCreate(): Observable<any> {
    let _obs;
    if (this._isLoggedIn) {
      _obs = this.dialog.open(WasAlert, {
        data: { title: 'Already Logged in', body: 'Do you wish to log out?', style: 'WasAlertStyleWarning' }
      }).afterClosed().pipe(map(retVal => retVal), mergeMap(result => {
        if (result === 1) {
          console.log('log out this user');
          this.logout();
          return observableOf(null);
        } else {
          return observableOf(null);
        }
      }), share());
    } else {
      _obs = this.dialog.open(LctCreate, { data: {} }).afterClosed();
    }
    return _obs;
  }
  /**
   * Open Create Account if not logged in, else confirm logout.
  */
  openAccountCreate(): Observable<any> {
    const _obs = this._openAccountCreate();
    _obs.subscribe(_ret => { });
    return _obs;
  }
  /**
   * Open WasProfile which shows Help or Account Info
   * @ignore
  */
  openuserinfo() {
    // this.dialog.open(WasProfile);
  }


}
