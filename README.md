**Lockwood Tools**

This will host, hopefully, useful tools for things at Lockwook Community Church.
The first project will be a ppt creator.

---

## PPT Creater

You can find this project at [lockwood.michaeljwickey.com/ppt](https://lockwood.michaeljwickey.com/ppt).

1. Click on **Song Name** and add begin typing and it will show autocomplete of songs already in the database. If song is found, simply click the autocomplete, else complete the title.
2. Click on the text area and edit the text, or if no song was found simply paste in the song text.
3. If the song is new and should be added to the database, simly check the **Add To Database** checkbox.
4. If desired, check the *optional*  **Append Blank** and/or **Prepend Blank** checkbox(es).
5. Click **Add Section**, if another section (song, scripture, or any text) is desired.
6. Click **Create PPT** to create the ppt.

NOTE: Creating a ppt will save all the text that went to create that ppt into the db so the ppt could be re-created later if desired.

---

# LockwoodFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
Use prefix lct (Lockwood Church Tools) for naming (e.g. lct-section)

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
